package cc.emmert.tiscreate.createaddition;

import java.util.Objects;
import java.util.Optional;

import com.mrh0.createaddition.blocks.modular_accumulator.ModularAccumulatorTileEntity;

import li.cil.tis3d.api.serial.SerialInterface;
import li.cil.tis3d.api.serial.SerialInterfaceProvider;
import li.cil.tis3d.api.serial.SerialProtocolDocumentationReference;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.level.Level;
import net.minecraftforge.registries.ForgeRegistryEntry;

public class ModularAccumulatorSerialInterfaceProvider extends ForgeRegistryEntry<SerialInterfaceProvider> implements SerialInterfaceProvider {

    private static final TranslatableComponent DOCUMENTATION_TITLE = new TranslatableComponent("block.createaddition.modular_accumulator");
    private static final String DOCUMENTATION_LINK = "modular_accumulator.md";
    private static final SerialProtocolDocumentationReference DOCUMENTATION_REFERENCE = new SerialProtocolDocumentationReference(DOCUMENTATION_TITLE, DOCUMENTATION_LINK);

    @Override
    public Optional<SerialProtocolDocumentationReference> getDocumentationReference() {
        return Optional.of(DOCUMENTATION_REFERENCE);
    }

    @Override
    public Optional<SerialInterface> getInterface(Level level, BlockPos pos, Direction dir) {
        final ModularAccumulatorTileEntity cannonMount = Objects.requireNonNull((ModularAccumulatorTileEntity) level.getBlockEntity(pos));
        return Optional.of(new ModularAccumulatorSerialInterface(cannonMount));
    }

    @Override
    public boolean matches(Level level, BlockPos pos, Direction dir) {
        return level.getBlockEntity(pos) instanceof ModularAccumulatorTileEntity;
    }

    @Override
    public boolean stillValid(Level level, BlockPos pos, Direction dir, SerialInterface serialInterface) {
        return serialInterface instanceof ModularAccumulatorSerialInterface;
    }
    
    private class ModularAccumulatorSerialInterface implements SerialInterface {

        private ModularAccumulatorTileEntity accumulator;

        public ModularAccumulatorSerialInterface(ModularAccumulatorTileEntity acc) {
            this.accumulator = acc;
        }

        @Override
        public boolean canRead() {
            return true;
        }

        @Override
        public boolean canWrite() {
            return false;
        }

        @Override
        public void load(CompoundTag arg0) {
        }

        @Override
        public short peek() {
            int energy = this.accumulator.getControllerTE().getEnergy(0).getEnergyStored();
            return (short) Math.min(energy/1000,32_767);
        }

        @Override
        public void reset() {
        }

        @Override
        public void save(CompoundTag arg0) {
        }

        @Override
        public void skip() {
        }

        @Override
        public void write(short arg0) {
        }

    }
}

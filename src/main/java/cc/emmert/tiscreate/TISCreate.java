package cc.emmert.tiscreate;

import cc.emmert.tiscreate.create.ContentObserverSerialInterfaceProvider;
import cc.emmert.tiscreate.create.SequencedGearshiftSerialInterfaceProvider;
import cc.emmert.tiscreate.create.SpeedControllerSerialInterfaceProvider;
import cc.emmert.tiscreate.create.SpeedGaugeSerialInterfaceProvider;
import cc.emmert.tiscreate.create.StockpileSwitchSerialInterfaceProvider;
import cc.emmert.tiscreate.create.StressGaugeSerialInterfaceProvider;
import cc.emmert.tiscreate.createaddition.ModularAccumulatorSerialInterfaceProvider;
import cc.emmert.tiscreate.createbigcannons.CannonMountSerialInterfaceProvider;
import cc.emmert.tiscreate.createbigcannons.YawControllerSerialInterfaceProvider;
import li.cil.tis3d.api.serial.SerialInterfaceProvider;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.ModList;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;

@Mod("tiscreate")
public class TISCreate {

    public TISCreate() {
        final DeferredRegister<SerialInterfaceProvider> serialInterfaces = DeferredRegister.create(SerialInterfaceProvider.REGISTRY, "tiscreate");

        serialInterfaces.register("content_observer", ContentObserverSerialInterfaceProvider::new);
        serialInterfaces.register("stress_gauge", StressGaugeSerialInterfaceProvider::new);
        serialInterfaces.register("speed_gauge", SpeedGaugeSerialInterfaceProvider::new);
        serialInterfaces.register("stockpile_switch", StockpileSwitchSerialInterfaceProvider::new);
        serialInterfaces.register("speed_controller",SpeedControllerSerialInterfaceProvider::new);
        serialInterfaces.register("sequenced_gearshift", SequencedGearshiftSerialInterfaceProvider::new);

        if(ModList.get().isLoaded("createaddition")) {
            serialInterfaces.register("modular_accumulator",ModularAccumulatorSerialInterfaceProvider::new);
        }

        if(ModList.get().isLoaded("createbigcannons")) {
            serialInterfaces.register("cannon_mount",CannonMountSerialInterfaceProvider::new);
            serialInterfaces.register("yaw_controller",YawControllerSerialInterfaceProvider::new);
        }

        serialInterfaces.register(FMLJavaModLoadingContext.get().getModEventBus());

        MinecraftForge.EVENT_BUS.register(this);
    }
}
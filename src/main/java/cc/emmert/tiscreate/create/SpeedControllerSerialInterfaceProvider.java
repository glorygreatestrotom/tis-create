package cc.emmert.tiscreate.create;

import java.lang.reflect.Field;
import java.util.Objects;
import java.util.Optional;

import com.simibubi.create.content.contraptions.relays.advanced.SpeedControllerTileEntity;
import com.simibubi.create.foundation.tileEntity.behaviour.scrollvalue.ScrollValueBehaviour;

import li.cil.tis3d.api.serial.SerialInterface;
import li.cil.tis3d.api.serial.SerialInterfaceProvider;
import li.cil.tis3d.api.serial.SerialProtocolDocumentationReference;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.level.Level;
import net.minecraftforge.registries.ForgeRegistryEntry;

public class SpeedControllerSerialInterfaceProvider extends ForgeRegistryEntry<SerialInterfaceProvider> implements SerialInterfaceProvider {

    private static final TranslatableComponent DOCUMENTATION_TITLE = new TranslatableComponent("block.create.rotation_speed_controller");
    private static final String DOCUMENTATION_LINK = "speed_controller.md";
    private static final SerialProtocolDocumentationReference DOCUMENTATION_REFERENCE = new SerialProtocolDocumentationReference(DOCUMENTATION_TITLE, DOCUMENTATION_LINK);

    @Override
    public Optional<SerialProtocolDocumentationReference> getDocumentationReference() {
        return Optional.of(DOCUMENTATION_REFERENCE);
    }

    @Override
    public Optional<SerialInterface> getInterface(Level level, BlockPos pos, Direction dir) {
        final SpeedControllerTileEntity contentObserver = Objects.requireNonNull((SpeedControllerTileEntity) level.getBlockEntity(pos));
        return Optional.of(new SpeedControllerSerialInterface(contentObserver));
    }

    @Override
    public boolean matches(Level level, BlockPos pos, Direction dir) {
        return level.getBlockEntity(pos) instanceof SpeedControllerTileEntity;
    }

    @Override
    public boolean stillValid(Level level, BlockPos pos, Direction dir, SerialInterface serialInterface) {
        return serialInterface instanceof SpeedControllerSerialInterface;
    }
    
    private class SpeedControllerSerialInterface implements SerialInterface {

        private SpeedControllerTileEntity speedController;

        public SpeedControllerSerialInterface(SpeedControllerTileEntity sc) {
            this.speedController = sc;
        }

        @Override
        public boolean canRead() {
            return true;
        }

        @Override
        public boolean canWrite() {
            return true;
        }

        
        @Override
        public short peek() {
            return (short) Math.min(this.getTargetSpeed().getValue(),32_767);
        }

        @Override
        public void write(short value) {
            this.getTargetSpeed().setValue(value);
        }

        // WARNING: Hacky bullshit ahead. Tread with caution. Rewrite this as soon as a better option presents itself
        private ScrollValueBehaviour getTargetSpeed() {
            try {
                // Why can't this field have an accessor of some kind?
                Field targetSpeedField = SpeedControllerTileEntity.class.getDeclaredField("targetSpeed");
                targetSpeedField.setAccessible(true);
                return (ScrollValueBehaviour) targetSpeedField.get(this.speedController);

            } catch (Exception e) {
                // If this fails, it's a bug here, so just blow everything up
                throw new RuntimeException(e);
            }
        }

        @Override
        public void reset() {
        }

        @Override
        public void skip() {
        }

        @Override
        public void load(CompoundTag tag) {
        }

        @Override
        public void save(CompoundTag tag) {
        }

    }
}

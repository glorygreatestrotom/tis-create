package cc.emmert.tiscreate.create;

import java.util.Objects;
import java.util.Optional;

import com.simibubi.create.content.logistics.block.redstone.StockpileSwitchTileEntity;

import li.cil.tis3d.api.serial.SerialInterface;
import li.cil.tis3d.api.serial.SerialInterfaceProvider;
import li.cil.tis3d.api.serial.SerialProtocolDocumentationReference;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.level.Level;
import net.minecraftforge.registries.ForgeRegistryEntry;

public class StockpileSwitchSerialInterfaceProvider extends ForgeRegistryEntry<SerialInterfaceProvider> implements SerialInterfaceProvider {

    private static final TranslatableComponent DOCUMENTATION_TITLE = new TranslatableComponent("block.create.stockpile_switch");
    private static final String DOCUMENTATION_LINK = "stockpile_switch.md";
    private static final SerialProtocolDocumentationReference DOCUMENTATION_REFERENCE = new SerialProtocolDocumentationReference(DOCUMENTATION_TITLE, DOCUMENTATION_LINK);

    @Override
    public Optional<SerialProtocolDocumentationReference> getDocumentationReference() {
        return Optional.of(DOCUMENTATION_REFERENCE);
    }

    @Override
    public Optional<SerialInterface> getInterface(Level level, BlockPos pos, Direction dir) {
        final StockpileSwitchTileEntity stockpileSwitch = Objects.requireNonNull((StockpileSwitchTileEntity) level.getBlockEntity(pos));
        return Optional.of(new StockpileSwitchSerialInterface(stockpileSwitch));
    }

    @Override
    public boolean matches(Level level, BlockPos pos, Direction dir) {
        return level.getBlockEntity(pos) instanceof StockpileSwitchTileEntity;
    }

    @Override
    public boolean stillValid(Level level, BlockPos pos, Direction dir, SerialInterface serialInterface) {
        return serialInterface instanceof StockpileSwitchSerialInterface;
    }
    
    private class StockpileSwitchSerialInterface implements SerialInterface {

        private StockpileSwitchTileEntity stockpileSwitch;

        public StockpileSwitchSerialInterface(StockpileSwitchTileEntity s) {
            this.stockpileSwitch = s;
        }

        @Override
        public boolean canRead() {
            return true;
        }

        @Override
        public boolean canWrite() {
            return false;
        }

        @Override
        public short peek() {
            return (short) Math.round(this.stockpileSwitch.currentLevel * 100);
        }

        @Override
        public void reset() {}

        @Override
        public void skip() {}

        @Override
        public void write(short val) {}

        @Override
        public void load(CompoundTag tag) {}

        @Override
        public void save(CompoundTag tag) {}
    }
}
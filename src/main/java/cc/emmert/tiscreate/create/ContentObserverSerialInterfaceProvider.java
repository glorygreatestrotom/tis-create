package cc.emmert.tiscreate.create;

import java.util.Objects;
import java.util.Optional;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.registries.ForgeRegistryEntry;

import li.cil.tis3d.api.serial.SerialInterfaceProvider;
import li.cil.tis3d.api.machine.HaltAndCatchFireException;
import li.cil.tis3d.api.serial.SerialInterface;
import li.cil.tis3d.api.serial.SerialProtocolDocumentationReference;
import li.cil.tis3d.util.EnumUtils;

import com.simibubi.create.content.logistics.block.redstone.ContentObserverTileEntity;
import com.simibubi.create.foundation.tileEntity.behaviour.inventory.InvManipulationBehaviour;
import com.simibubi.create.foundation.tileEntity.behaviour.inventory.TankManipulationBehaviour;
import com.simibubi.create.foundation.tileEntity.behaviour.filtering.FilteringBehaviour;

public class ContentObserverSerialInterfaceProvider extends ForgeRegistryEntry<SerialInterfaceProvider> implements SerialInterfaceProvider {

    private static final TranslatableComponent DOCUMENTATION_TITLE = new TranslatableComponent("block.create.content_observer");
    private static final String DOCUMENTATION_LINK = "content_observer.md";
    private static final SerialProtocolDocumentationReference DOCUMENTATION_REFERENCE = new SerialProtocolDocumentationReference(DOCUMENTATION_TITLE, DOCUMENTATION_LINK);

    @Override
    public Optional<SerialProtocolDocumentationReference> getDocumentationReference() {
        return Optional.of(DOCUMENTATION_REFERENCE);
    }

    @Override
    public Optional<SerialInterface> getInterface(Level level, BlockPos pos, Direction dir) {
        final ContentObserverTileEntity contentObserver = Objects.requireNonNull((ContentObserverTileEntity) level.getBlockEntity(pos));
        return Optional.of(new ContentObserverSerialInterface(contentObserver));
    }

    @Override
    public boolean matches(Level level, BlockPos pos, Direction _dir) {
        return level.getBlockEntity(pos) instanceof ContentObserverTileEntity;
    }

    @Override
    public boolean stillValid(Level arg0, BlockPos _pos, Direction _dir, SerialInterface serialInterface) {
        return serialInterface instanceof ContentObserverSerialInterface;
    }

    private class ContentObserverSerialInterface implements SerialInterface {
        private static final String TAG_MODE = "mode";

        // For items, LOW mode calculates count mod 32,768, and
        // HIGH mode calculates count / 32,768, round down.
        // For fluids, HIGH is in buckets rounded down, LOW is remaining mB
        private enum MODE {
            HIGH,
            LOW,
        }

        private ContentObserverTileEntity contentObserver;
        private MODE mode;

        public ContentObserverSerialInterface(ContentObserverTileEntity contentObserver) {
            this.contentObserver = contentObserver;
            this.mode = MODE.LOW;
        }

        @Override
        public boolean canRead() {
            return true;
        }

        @Override
        public boolean canWrite() {
            return true;
        }

        @Override
        public short peek() {
            InvManipulationBehaviour invManipulationBehaviour = this.contentObserver.getBehaviour(InvManipulationBehaviour.TYPE);
		    FilteringBehaviour filteringBehaviour = this.contentObserver.getBehaviour(FilteringBehaviour.TYPE);
		    IItemHandler handler = invManipulationBehaviour.getInventory();

            if (handler == null)
                // No attached inventory, try reading a fluid
                return readFluid();

            long collected = 0;
            for (int i = 0; i < handler.getSlots(); i++) {
                ItemStack stack = handler.extractItem(i, handler.getSlotLimit(i), true);
                if (stack.isEmpty())
                    continue;
                if (!filteringBehaviour.test(stack))
                    continue;
                collected += stack.getCount();
            }

            switch(this.mode) {
                case HIGH:
                    collected /= 32_768;
                case LOW:
                    collected %= 32_768;
            }

            return (short) Math.min(collected, 32_767);
        }

        private short readFluid() {
            TankManipulationBehaviour tankManipulationBehaviour = this.contentObserver.getBehaviour(TankManipulationBehaviour.OBSERVE);
		    FilteringBehaviour filteringBehaviour = this.contentObserver.getBehaviour(FilteringBehaviour.TYPE);
		    IFluidHandler handler = tankManipulationBehaviour.getInventory();

            if (handler == null) 
                // Can't read a fluid or an item? Throw a fit, that's undefined behavior
                throw new HaltAndCatchFireException();

            long collected = 0;
            for (int i = 0; i < handler.getTanks(); i++) {
                FluidStack stack = handler.getFluidInTank(i);
                if (stack.isEmpty())
                    continue;
                if (!filteringBehaviour.test(stack))
                    continue;
                collected += stack.getAmount();
            }

            switch(this.mode) {
                case HIGH:
                    collected /= 1000;
                case LOW:
                    collected %= 1000;
            }

            return (short) Math.min(collected, 32_767);
        }

        @Override
        public void reset() {
            this.mode = MODE.LOW;
        }

        @Override
        public void skip() {           
        }

        @Override
        public void write(short value) {
            if(value == 0) {
                this.mode = MODE.LOW;
            }
            else if(value == 1) {
                this.mode = MODE.HIGH;
            }
        }

        @Override
        public void load(final CompoundTag tag) {
            mode = EnumUtils.load(ContentObserverSerialInterface.MODE.class, TAG_MODE, tag);
        }

        @Override
        public void save(final CompoundTag tag) {
            EnumUtils.save(mode, TAG_MODE, tag);
        }
    }
}

package cc.emmert.tiscreate.create;

import java.util.Objects;
import java.util.Optional;

import com.simibubi.create.content.logistics.block.belts.tunnel.BeltTunnelTileEntity;

import li.cil.tis3d.api.serial.SerialInterface;
import li.cil.tis3d.api.serial.SerialInterfaceProvider;
import li.cil.tis3d.api.serial.SerialProtocolDocumentationReference;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.level.Level;
import net.minecraftforge.registries.ForgeRegistryEntry;


// NOTE: Implemention is on hold pending figuring out how to get data from Create. DO NOT REGISTER
public class TunnelSerialInterfaceProvider extends ForgeRegistryEntry<SerialInterfaceProvider> implements SerialInterfaceProvider {

    @Override
    public Optional<SerialProtocolDocumentationReference> getDocumentationReference() {
        return Optional.empty();
    }

    @Override
    public Optional<SerialInterface> getInterface(Level level, BlockPos pos, Direction dir) {
        final BeltTunnelTileEntity tunnel = Objects.requireNonNull((BeltTunnelTileEntity) level.getBlockEntity(pos));
        return Optional.of(new TunnelSerialInterface(tunnel));
    }

    @Override
    public boolean matches(Level level, BlockPos pos, Direction dir) {
        return level.getBlockEntity(pos) instanceof BeltTunnelTileEntity;
    }

    @Override
    public boolean stillValid(Level level, BlockPos pos, Direction dir, SerialInterface serialInterface) {
        return serialInterface instanceof TunnelSerialInterface;
    }
    
    private class TunnelSerialInterface implements SerialInterface {
        private static final String TAG_ACCUMULATED_COUNT = "count";

        // private BeltTunnelTileEntity tunnel;
        private short accumulatedCount;

        public TunnelSerialInterface(BeltTunnelTileEntity t) {
            // this.tunnel = t;
            this.accumulatedCount = 0;
        }

        @Override
        public boolean canRead() {
            return true;
        }

        @Override
        public boolean canWrite() {
            return true;
        }

        @Override
        public short peek() {
            return -1;
        }

        @Override
        public void reset() {
            this.accumulatedCount = 0;
        }

        @Override
        public void skip() {}

        @Override
        public void write(short val) {
            this.accumulatedCount = 0;
        }

        @Override
        public void load(final CompoundTag tag) {
            this.accumulatedCount = (short) tag.getShort(TAG_ACCUMULATED_COUNT);
        }

        @Override
        public void save(final CompoundTag tag) {
            tag.putShort(TAG_ACCUMULATED_COUNT, accumulatedCount);
        }
    }
}
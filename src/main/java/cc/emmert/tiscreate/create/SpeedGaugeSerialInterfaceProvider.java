package cc.emmert.tiscreate.create;

import java.util.Objects;
import java.util.Optional;

import com.simibubi.create.content.contraptions.relays.gauge.SpeedGaugeTileEntity;

import li.cil.tis3d.api.serial.SerialInterface;
import li.cil.tis3d.api.serial.SerialInterfaceProvider;
import li.cil.tis3d.api.serial.SerialProtocolDocumentationReference;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.level.Level;
import net.minecraftforge.registries.ForgeRegistryEntry;

public class SpeedGaugeSerialInterfaceProvider extends ForgeRegistryEntry<SerialInterfaceProvider> implements SerialInterfaceProvider {

    private static final TranslatableComponent DOCUMENTATION_TITLE = new TranslatableComponent("block.create.speedometer");
    private static final String DOCUMENTATION_LINK = "speed_gauge.md";
    private static final SerialProtocolDocumentationReference DOCUMENTATION_REFERENCE = new SerialProtocolDocumentationReference(DOCUMENTATION_TITLE, DOCUMENTATION_LINK);

    @Override
    public Optional<SerialProtocolDocumentationReference> getDocumentationReference() {
        return Optional.of(DOCUMENTATION_REFERENCE);
    }

    @Override
    public Optional<SerialInterface> getInterface(Level level, BlockPos pos, Direction dir) {
        final SpeedGaugeTileEntity gauge = Objects.requireNonNull((SpeedGaugeTileEntity) level.getBlockEntity(pos));
        return Optional.of(new SpeedGaugeSerialInterface(gauge));
    }

    @Override
    public boolean matches(Level level, BlockPos pos, Direction dir) {
        return level.getBlockEntity(pos) instanceof SpeedGaugeTileEntity;
    }

    @Override
    public boolean stillValid(Level level, BlockPos pos, Direction dir, SerialInterface serialInterface) {
        return serialInterface instanceof SpeedGaugeSerialInterface;
    }
    
    private class SpeedGaugeSerialInterface implements SerialInterface {

        private SpeedGaugeTileEntity gauge;

        public SpeedGaugeSerialInterface(SpeedGaugeTileEntity g) {
            this.gauge = g;
        }

        @Override
        public boolean canRead() {
            return true;
        }

        @Override
        public boolean canWrite() {
            return false;
        }

        @Override
        public short peek() {
            return (short) Math.min(Math.round(this.gauge.getSpeed()),32_767);
        }

        @Override
        public void reset() {}

        @Override
        public void skip() {}

        @Override
        public void write(short val) {}

        @Override
        public void load(CompoundTag tag) {}

        @Override
        public void save(CompoundTag tag) {}
    }
}

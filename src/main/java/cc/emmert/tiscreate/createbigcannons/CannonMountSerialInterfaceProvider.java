package cc.emmert.tiscreate.createbigcannons;

import java.util.Objects;
import java.util.Optional;

import li.cil.tis3d.api.serial.SerialInterface;
import li.cil.tis3d.api.serial.SerialInterfaceProvider;
import li.cil.tis3d.api.serial.SerialProtocolDocumentationReference;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.level.Level;
import net.minecraftforge.registries.ForgeRegistryEntry;
import rbasamoyai.createbigcannons.cannon_control.cannon_mount.CannonMountBlockEntity;

public class CannonMountSerialInterfaceProvider extends ForgeRegistryEntry<SerialInterfaceProvider> implements SerialInterfaceProvider {

    private static final TranslatableComponent DOCUMENTATION_TITLE = new TranslatableComponent("block.createbigcannons.cannon_mount");
    private static final String DOCUMENTATION_LINK = "cannon_mount.md";
    private static final SerialProtocolDocumentationReference DOCUMENTATION_REFERENCE = new SerialProtocolDocumentationReference(DOCUMENTATION_TITLE, DOCUMENTATION_LINK);

    @Override
    public Optional<SerialProtocolDocumentationReference> getDocumentationReference() {
        return Optional.of(DOCUMENTATION_REFERENCE);
    }

    @Override
    public Optional<SerialInterface> getInterface(Level level, BlockPos pos, Direction dir) {
        final CannonMountBlockEntity cannonMount = Objects.requireNonNull((CannonMountBlockEntity) level.getBlockEntity(pos));
        return Optional.of(new CannonMountSerialInterface(cannonMount));
    }

    @Override
    public boolean matches(Level level, BlockPos pos, Direction dir) {
        return level.getBlockEntity(pos) instanceof CannonMountBlockEntity;
    }

    @Override
    public boolean stillValid(Level level, BlockPos pos, Direction dir, SerialInterface serialInterface) {
        return serialInterface instanceof CannonMountSerialInterface;
    }
    
    private class CannonMountSerialInterface implements SerialInterface {

        private CannonMountBlockEntity mount;

        public CannonMountSerialInterface(CannonMountBlockEntity mount) {
            this.mount = mount;
        }

        @Override
        public boolean canRead() {
            return true;
        }

        @Override
        public boolean canWrite() {
            return false;
        }

        @Override
        public short peek() {
            float pitch = this.mount.getPitchOffset(0.0f);
            return (short) Math.floor(pitch*10.0f);
        }

        @Override
        public void reset() {}

        @Override
        public void skip() {}

        @Override
        public void write(short val) {}

        @Override
        public void load(CompoundTag tag) {}

        @Override
        public void save(CompoundTag tag) {}
    }
}
